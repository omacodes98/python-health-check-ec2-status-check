# Python Health Check EC2 Status Checks 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Created EC2 instance with Terraform 

* Wrote a Python script that fetches statuses of EC2 instances and prints to the console 

* Extended the Python script to continuously check the status of EC2 Instances in a specific interval 


## Technologies Used 

* Python 

* Boto3 

* AWS 

* Terraform 

## Steps 

Step 1: Create a terraform configuration file to deploy three ec2 instances 

[Terraform Configuration File](/images/01_create_a_terraform_configuration_file_to_deploy_three_ec2_instance.png)

Step 2: Use Terraform init to initialize working directory and install necessarry plugins

    terraform init

[Terraform init](/images/02_use_terraform_init_to_initialize_working_directory_and_install_necessary_plugins.png)

Step 3: Perform terraform plan to see components that will be created 

    terraform plan -var-file terraform-dev.tfvars

[Terraform plan](/images/03_perform_terraform_plan_to_see_components_that_will_be_created.png)

Step 4: Perform terraform apply to get terraform from current state to desired state in your configuration file 

    terraform apply -var-file terraform-dev.tfvars

[Terraform apply](/images/04_apply_changes.png)
[Instances running](/images/05_instances_running_on_aws.png)

Step 5: Impott boto3 library in python file 

[Import boto3](/images/06_import_boto3_library.png)

Step 6: Use function describe_instance which gives all the instances in a specific region

[Describe Instances](/images/07_use_function_describe_instance_which_gives_all_the_instances_in_specific_region.png)

Step 7: Save response into a variable 

[describe instance variable](/images/08_save_into_a_variable.png)

Step 8: Use for loop to loop through instances 

[for loop](/images/09_use_for_loop_to_loop_through_instances.png)

Step 9: Save result of for loop into a variable this is because results are instance elements 

[Variable for instance elements](/images/10_save_the_result_of_the_loop_into_a_variable_the_results_are_instance_elements.png)

Step 10: Loop through instance elements variable as this will then give a dictionary not a list of dictionaries 

[Looping through instance elements](/images/11_loop_through_instance_result_variable_as_this_will_then_give_a_dictionary_not_a_list_of_dictionaries.png)

Step 11: in the for loop print state and id for every instance 
[print instance state and id](/images/12_in_for_loop_print_state_and_id_for_every_instance.png)

Step 12: Call the function describe instance status for ec2 client and save to variale 

[describe instance status](/images/13_call_the_function_describe_instance_status_for_ec2_client_and_save_to_variable_.png)

Step 13: Write a for loop that can access system status and instance status through dictionary hierachy 

[for lopp for sys status and instance status](/images/14_write_a_for_loop_that_can_access_sys_status_and_instance_state_through_dictionary_hierachy.png)

Step 14: Delete describe instance function and logic related to it because we can find instance status with descirbe insstance status function 

[Delete describe instance function](/images/15_delete_describe_instance_function_and_logic_related_to_it_because_we_can_find_instance_status_with_describe_instance_status_function.png)

Step 15: Install schedule with pip this is to make our program run every 5 minutes 

[Install schedule](/images/16_install_schedule_with_pip_this_is_to_make_our_programs_run_every_5_minutes.png)

Step 16: Import schedule 

[Import schedule](/images/17_import_schedule.png)

Step 17: Put all logic related to checking status inside a function 

[create a function for checking status](/images/18_all_logic_related_to_checking_status_inside_a_function.png)

Step 18: Set schedule to run every 5 minutes with appropriate syntax

[Schedule for 5 minutes](/images/19_set_schedule_to_run_every_5_minutes_with_appropriate_syntax_can_be_seen_in_documentation_of_library.png)

Step 19: Run in a while loop to enable it to run forever

[Put it in a while loop](/images/20_run_it_in_a_while_loop_to_enable_it_to_run_forever.png)


## Installation 

    brew install terraform 

    brew install python3 

## Usage 

    terraform apply -var-file terraform-dev.tfvars

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/python-health-check-ec2-status-check.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/python-health-check-ec2-status-check

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.